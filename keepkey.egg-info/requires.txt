ecdsa>=0.9
protobuf>=3.0.0
mnemonic>=0.8
hidapi>=0.7.99.post15
libusb1>=1.6

[ethereum]
rlp>=0.4.4
ethjsonrpc>=0.3.0
